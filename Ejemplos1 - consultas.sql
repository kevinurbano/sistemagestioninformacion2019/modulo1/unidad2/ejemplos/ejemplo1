﻿USE concursos;

/*
  1- Indicar el nombre de los concursantes de burgos
*/

SELECT 
    DISTINCT c.nombre 
  FROM 
    concursantes c 
  WHERE
    c.provincia='Burgos';

/*
  2- Indicar cuantos concursantes hay de burgos
*/

SELECT 
  COUNT(*) numConcursantes 
FROM 
  concursantes c 
WHERE 
  c.provincia='Burgos';

/*
  3- Indicar cuantos concursantes hay de cada poblacion  
*/
SELECT 
  c.poblacion,COUNT(*) 
FROM 
  concursantes c 
GROUP BY 
  c.poblacion;

/*
  4- Indicar las poblaciones que tienen concursantes de menos de 90kg
*/
SELECT
  DISTINCT c.poblacion 
FROM 
  concursantes c 
WHERE 
  c.peso<90;

/*
  5- Indicar las poblaciones que tienen mas de 1 concursante
*/
SELECT 
  c.poblacion 
FROM 
  concursantes c 
GROUP BY 
  c.poblacion 
HAVING 
  COUNT(*)>1;

/*
  6- Indicar las poblaciones que tienen mas de 1 concursante de menos de 90kg  
*/
SELECT 
  c.poblacion 
FROM 
  concursantes c 
WHERE 
  c.peso<90 
GROUP BY 
  c.poblacion 
HAVING
  COUNT(*)>1;

SELECT * FROM concursantes c;