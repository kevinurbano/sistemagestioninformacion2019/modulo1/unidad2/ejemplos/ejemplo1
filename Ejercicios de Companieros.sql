﻿USE concursos;

/* 1 - WHERE
       Listar los concursantes nacidos en 1981.*/
SELECT 
  * 
  FROM 
    concursantes c 
  WHERE 
    year(c.fechaNacimiento)=1981;

/* 2 - HAVING
       Listar las provincicias que tengan más de 1 concursante. */
SELECT 
  c.provincia 
  FROM 
    concursantes c 
  GROUP BY 
    c.provincia 
  HAVING 
    COUNT(*)>1;

/* 3 - WHERE + HAVING 
       Listar el nombre de los concursantes cuya provincia tenga 
       más de 1 concursante y hayan nacido entre 1978 y 1987. */
/* C1 - Provincias que tengan mas de una provincia */
SELECT 
  c.provincia 
  FROM 
    concursantes c 
  GROUP BY 
    c.provincia 
  HAVING 
    COUNT(*)>1;

SELECT DISTINCT c.nombre FROM concursantes c 
  JOIN (SELECT 
          c.provincia 
          FROM 
            concursantes c        
          GROUP BY 
            c.provincia 
          HAVING 
            COUNT(*)>1) c1 USING(provincia) WHERE YEAR(c.fechaNacimiento) BETWEEN 1978 AND 1987;

/* 4 - FUNCION DE TOTALES 
       Altura media de los concursantes de santander */
SELECT 
  AVG(altura) alturaMedia 
  FROM 
    concursantes 
  WHERE 
    poblacion='santander';

/* 5 - 3+4 
       Altura media de los concursantes de santander que hayan nacido entre 1978 y 1987
       y cuya provincia tenga más de 1 concursante */
SELECT 
  AVG(c.altura) alturaMedia
FROM 
  concursantes c 
WHERE 
  c.poblacion='santander' AND YEAR(c.fechaNacimiento) BETWEEN 1978 AND 1987;
